package tomia.prov.mstr.sso;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ProvUser implements Serializable {

	private String status;
	private int userId;
	private int accountId;
	private String username;
	private String userToken;
	private Map<Integer, String> rolesById = new HashMap<>();
	private Map<String, Integer> rolesByName = new HashMap<>();

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getToken() {
		return userToken;
	}

	public void generateToken(String prefix) throws NoSuchAlgorithmException {
		userToken = sha256(String.format("%s#%d#%d#%s", prefix, userId, accountId, username));
	}

	public Map<Integer, String> getRolesById()
	{
		return this.rolesById;
	}

	public boolean hasRoleById(Integer roleId)
	{
		return this.rolesById.containsKey(roleId);
	}

	public Map<String, Integer> getRolesByName()
	{
		return this.rolesByName;
	}

	public boolean hasRoleByName(String role)
	{
		return this.rolesByName.containsKey(role);
	}

	public void addRole(Integer roleId, String role)
	{
		this.rolesById.put(roleId, role);
		this.rolesByName.put(role, roleId);
	}

	public String getRoleNameById(Integer roleId)
	{
		if(this.rolesById.containsKey(roleId)) {
			return this.rolesById.get(roleId);
		}
		return null;
	}

	public Integer getRoleIdByName(String role)
	{
		if(this.rolesByName.containsKey(role)) {
			return this.rolesByName.get(role);
		}
		return null;
	}

	@Override
	public String toString() {
		return new StringBuilder()
			.append("status: ").append(status).append("\n")
			.append("userId: ").append(userId).append("\n")
			.append("accountId: ").append(accountId).append("\n")
			.append("username: ").append(username).append("\n")
			.append("userToken: ").append(userToken).append("\n")
			.append("rolesById: ").append(rolesById.toString()).append("\n")
			.append("rolesByName: ").append(rolesByName.toString()).append("\n")
			.toString();
	}

	private final static char[] DIGITS_LOWER = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd','e', 'f' };

	public static char[] encodeHex(final byte[] data)
	{
		final int l = data.length;
		final char[] out = new char[l << 1];
		for (int i = 0, j = 0; i < l; i++) {
			out[j++] = DIGITS_LOWER[(0xF0 & data[i]) >>> 4];
			out[j++] = DIGITS_LOWER[0x0F & data[i]];
		}
		return out;
	}

	public static String sha256(String data) throws NoSuchAlgorithmException
	{
		return new String(encodeHex(MessageDigest.getInstance("SHA-256").digest(data.getBytes(StandardCharsets.UTF_8))));
	}

	public static void main(String[] args) throws Exception {
		if(args.length < 3) {
			System.out.println("Missing arguments!");
			System.out.println("Usage: java -cp WEB-INF/classes tomia.prov.mstr.sso.ProvUser <userId> <accountId> <userName>");
			return;
		}
		int userId = Integer.parseInt(args[0]);
		int accountId = Integer.parseInt(args[1]);
		String userName = args[2];
		System.out.println("User ID: " + userId);
		System.out.println("Account ID: " + accountId);
		System.out.println("User Name: " + userName);
		Properties props = new Properties();
		System.out.println("Loading " + ProvUser.class.getClassLoader().getResource("mstr-prov-sso.properties"));
		props.load(ProvUser.class.getClassLoader().getResourceAsStream("mstr-prov-sso.properties"));
		String tokenPrefix = props.getProperty("SSO_HASH_RS");
		System.out.println("Token Prefix: " + tokenPrefix);
		ProvUser user = new ProvUser();
		user.setUserId(userId);
		user.setAccountId(accountId);
		user.setUsername(userName);
		user.generateToken(tokenPrefix);
		System.out.println("User Token: " + user.getToken());
	}

}
