package tomia.prov.mstr.sso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.net.ssl.SSLException;

import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;

import com.microstrategy.utils.log.Level;
import com.microstrategy.utils.serialization.EnumWebPersistableState;
import com.microstrategy.web.app.AbstractExternalSecurity;
import com.microstrategy.web.beans.RequestKeys;
import com.microstrategy.web.objects.WebIServerSession;
import com.microstrategy.web.objects.WebObjectsException;
import com.microstrategy.web.objects.WebObjectsFactory;
import com.microstrategy.web.platform.ContainerServices;
import com.microstrategy.web.platform.GenericCookie;
import com.microstrategy.webapi.EnumDSSXMLApplicationType;
import com.microstrategy.webapi.EnumDSSXMLAuthModes;
import reactor.netty.http.client.HttpClient;

import static java.util.Collections.singletonMap;

import static com.microstrategy.utils.StringUtils.isEmpty;
import static com.microstrategy.utils.StringUtils.isNotEmpty;

public class CustomSSO extends AbstractExternalSecurity {

	private static final MstrLogWrapper LOG = new MstrLogWrapper(CustomSSO.class);

	private static final String PROPERTIES_FILE_NAME = "mstr-prov-sso.properties";
	private static final String SESSION_SSO_USER_TOKEN = "PROV_SSO_USER_TOKEN";
	private static final String SESSION_STATE = "SESSION_STATE";

	private static final String MSTR_SERVER_KEY = "server";
	private static final String MSTR_PROJECT_KEY = "project";
	private static final String MSTR_PORT_KEY = "port";

	private static final String PORTAL_KEY = "portal";

	private static WebClient webClient;
	// prov sso settings located in the ini file
	private static int debugLevel = 5;
	private static String ssoWebServiceUrl;
	private static boolean ssoVerifySsl;
	private static String ssoSecretKey;
	private static String ssoCookieName;
	private static String ssoLoginPage;
	private static String ssoErrorPage;
	private static String ssoTokenPrefix;
	private static boolean ssoEnableRoleCheck;
	private static String ssoRoleName;
	private static boolean allowMstrDefaultLogin;
	private static String ssoMstrServerDefaultValue;
	private static String ssoMstrProjectDefaultValue;
	private static int ssoMstrPortDefaultValue;
	private static String ssoMstrServerToken;
	private static String debugLogFile;

	static {
		String ssoConfigurationFile = CustomSSO.class
				.getResource("CustomSSO.class")
				.getPath()
				.replace("tomia/prov/mstr/sso/CustomSSO.class", "")
				+ PROPERTIES_FILE_NAME;

		debugLogFile = CustomSSO.class
				.getResource("CustomSSO.class")
				.getPath()
				.replace("tomia/prov/mstr/sso/CustomSSO.class", "")
				+ "mstr_prov_sso_debug.log";

		System.out.println("debugLogFile = " + debugLogFile);

		try (InputStream input = new FileInputStream(ssoConfigurationFile)) {
			Properties props = new Properties();
			props.load(input);
			debugLevel = Integer.parseInt(props.getProperty("PROV_CUSTOM_SSO_DEBUG_LEVEL") );
			ssoWebServiceUrl = props.getProperty("PROV_URI");
			ssoVerifySsl = Boolean.parseBoolean(props.getProperty("SSL_VERIFICATION") );
			ssoSecretKey = props.getProperty("INTERNAL_SECRET_KEY");
			ssoCookieName = props.getProperty("PROV_SSO_COOKIE_NAME");
			ssoLoginPage = props.getProperty("PROV_LOGIN_PAGE");
			ssoErrorPage = props.getProperty("PROV_FAILURE_PAGE");
			ssoTokenPrefix = props.getProperty("SSO_HASH_RS");
			ssoEnableRoleCheck = Boolean.parseBoolean(props.getProperty("PROV_ENABLE_ROLE_CHECK") );
			ssoRoleName = props.getProperty("PROV_MSTR_ROLE");
			allowMstrDefaultLogin = Boolean.parseBoolean(props.getProperty("MSTR_ALLOW_DEFAULT_LOGIN") );
			ssoMstrServerDefaultValue = props.getProperty("MSTR_SERVER_DEFAULT_VALUE");
			ssoMstrProjectDefaultValue = props.getProperty("MSTR_PROJECT_DEFAULT_VALUE");
			ssoMstrPortDefaultValue = Integer.parseInt(props.getProperty("MSTR_ISERVER_DEFAULT_PORT") );
			ssoMstrServerToken = props.getProperty("MSTR_SERVER_TRUST_TOKEN");
			webClient = createWebClient();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	static WebClient createWebClient() throws SSLException {
		if(ssoVerifySsl) return WebClient.create();
		SslContext sslContext = SslContextBuilder.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build();
		HttpClient httpClient = HttpClient.create().secure(scp -> scp.sslContext(sslContext));
		return WebClient.builder().clientConnector(new ReactorClientHttpConnector(httpClient)).build();
	}

	@Override
	public int handlesAuthenticationRequest(RequestKeys reqKeys, ContainerServices cntSvcs, int reason) {
		String methodName = "handlesAuthenticationRequest";
		if(debugLevel > 4) {
			writeDebugMsg(methodName + " called, reason=" + reason + "; RequestKeys=" + reqKeys + "\n");
			LOG.logger.logp(Level.INFO, LOG.logClassName, methodName, methodName + " invoked.");
			LOG.logger.entering(LOG.logClassName, methodName, new Object[]{reqKeys, cntSvcs, reason});
		}
		if(isNotEmpty(reqKeys.getValue(PORTAL_KEY))) {
			if(debugLevel > 4) writeDebugMsg(methodName + " portal parameter is available, returning " + COLLECT_SESSION_NOW + "\n");
			return COLLECT_SESSION_NOW;
		}
		int res = allowMstrDefaultLogin ? USE_MSTR_DEFAULT_LOGIN : USE_CUSTOM_LOGIN_PAGE;
		if(debugLevel > 4) writeDebugMsg(methodName + " returning " + res + "\n");
		return res;
	}

    @Override
    public WebIServerSession getWebIServerSession(RequestKeys reqKeys, ContainerServices cntSvcs)
	{
		String methodName = "getWebIServerSession";
		if(debugLevel>=5) {
			LOG.logger.logp(Level.INFO, LOG.logClassName, methodName, methodName + " invoked.");
			LOG.logger.entering(LOG.logClassName, methodName, new Object[]{reqKeys, cntSvcs});
		}

		StringBuffer logBuffer = new StringBuffer();
		if(debugLevel>0) {
			logBuffer.append("getWebIServerSession invoked")
					.append("\n")
					.append(reqKeys.toString())
					.append("\n");
		}

		// Verify if we already have valid session

		String sessionState = (String) cntSvcs.getSessionAttribute(SESSION_STATE);
		if (isNotEmpty(sessionState)) {
			logBuffer.append("sessionState = ")
					.append(sessionState)
					.append("\n");

			WebIServerSession webIServerSession = WebObjectsFactory.getInstance().getIServerSession();
			if(webIServerSession.restoreState(sessionState)) {
				try {
					if (webIServerSession.isAlive() && webIServerSession.isActive()) {
						logBuffer.append("sessionState isActive() && isAlive()")
								.append("\n");
						writeDebugMsg(logBuffer.toString());
						return webIServerSession;
					}
					else {
						logBuffer.append("sessionState IS NOT isActive() && isAlive()")
								.append("\n");
						closeSession(webIServerSession);
					}
				}
				catch (WebObjectsException e) {
					// we close current and try to get new one
					LOG.error(methodName, "Exception during WebIServerSession isAlive() && isActive check.", e);
					closeSession(sessionState);
				}
			}
		}

		// SESSION_STATE is not valid - check if we have prov token already in the session and use that, if not,
		// validate the user based on the cookie if present;

		String ssoUserToken = (String) cntSvcs.getSessionAttribute(SESSION_SSO_USER_TOKEN);
		if(isEmpty(ssoUserToken)) {
			ssoUserToken = validateSsoUser(cntSvcs);
		}

		logBuffer.append("ssoUserToken = ")
				.append(ssoUserToken)
				.append("\n");

		writeDebugMsg(logBuffer.toString());

		// validateSsoUser can return null value in case cookie wasn't found
		return isEmpty(ssoUserToken) ? null : createWebIServerSession(ssoUserToken, reqKeys, cntSvcs);
    }

	@Override
	public String getCustomLoginURL(String url, String server, int port, String project) {
		String methodName = "getCustomLoginURL";
		if(debugLevel > 4) {
			writeDebugMsg(methodName + " called, url=" + url + "; server=" + server + "; port=" + port + "; project=" + project + "\n");
			LOG.logger.logp(Level.INFO, LOG.logClassName, methodName, methodName + " invoked.");
			LOG.logger.entering(LOG.logClassName, methodName, new Object[]{url, server, port, project});
		}
		String res = isNotEmpty(ssoLoginPage) ? ssoLoginPage : super.getCustomLoginURL(url, server, port, project);
		if(debugLevel > 4) writeDebugMsg(methodName + " returning " + res + "\n");
		return res;
	}

	@Override
	public String getFailureURL(int reqType, ContainerServices cntSvcs) {
		String methodName = "getFailureURL";
		if(debugLevel > 4) {
			writeDebugMsg(methodName + " called, reqType=" + reqType + "\n");
			LOG.logger.logp(Level.INFO, LOG.logClassName, methodName, methodName + " invoked.");
			LOG.logger.entering(LOG.logClassName, methodName, new Object[]{reqType, cntSvcs});
		}
		String res = isNotEmpty(ssoErrorPage) ? ssoErrorPage : super.getFailureURL(reqType, cntSvcs);
		if(debugLevel > 4) writeDebugMsg(methodName + " returning " + res + "\n");
		return res;
	}

	// -----------------------------------------------------------------------------------------------------------------

	private WebIServerSession createWebIServerSession(String ssoUserToken, RequestKeys reqKeys, ContainerServices cntSvcs)
	{
		String methodName = "createWebIServerSession";
		if(debugLevel>=5) {
			LOG.logger.logp(Level.INFO, LOG.logClassName, methodName, methodName + " invoked.");
			LOG.logger.entering(LOG.logClassName, methodName, new Object[]{ssoUserToken, reqKeys, cntSvcs});
		}

		StringBuffer logBuffer = new StringBuffer();
		if(debugLevel>0) {
			logBuffer.append("createWebIServerSession invoked")
					.append("\n")
					.append("ssoUserToken = ")
					.append(ssoUserToken)
					.append("\n")
					.append(reqKeys.toString())
					.append("\n");
		}

		try {
			WebIServerSession webIServerSession = WebObjectsFactory.getInstance().getIServerSession();

			String server = isNotEmpty(reqKeys.getValue(MSTR_SERVER_KEY)) ? reqKeys.getValue(MSTR_SERVER_KEY) : ssoMstrServerDefaultValue;
			String project = isNotEmpty(reqKeys.getValue(MSTR_PROJECT_KEY)) ? reqKeys.getValue(MSTR_PROJECT_KEY) : ssoMstrProjectDefaultValue;
			int port = isNotEmpty(reqKeys.getValue(MSTR_PORT_KEY)) ? Integer.parseInt(reqKeys.getValue(MSTR_PORT_KEY)) : ssoMstrPortDefaultValue;

			if (debugLevel >= 0) {
				LOG.info(methodName, String.format("server=%s, project=%s, port=%s", server, project, port));
				logBuffer.append(String.format("server=%s, project=%s, port=%s", server, project, port))
						.append("\n");
			}

			webIServerSession.setServerName(server);
			webIServerSession.setProjectName(project);
			webIServerSession.setServerPort(port);
			webIServerSession.setAuthMode(EnumDSSXMLAuthModes.DssXmlAuthSimpleSecurityPlugIn);
			webIServerSession.setApplicationType(EnumDSSXMLApplicationType.DssXmlApplicationPortal);
			webIServerSession.setLDAPDistinguishedName(ssoUserToken);
			webIServerSession.setLogin(ssoUserToken);
			webIServerSession.setTrustToken(ssoMstrServerToken);

			if(debugLevel>=1) {
				LOG.info(methodName, String.format("Session %s created for userTokenId %s", webIServerSession.getSessionID(), ssoUserToken));
			}
			// enforce calling getSessionID
			logBuffer.append("session id = ")
					.append(webIServerSession.getSessionID())
					.append("\n");

			webIServerSession.getSessionID();
			// save WebIServerSession state into the current session
			cntSvcs.setSessionAttribute(SESSION_STATE, webIServerSession.saveState(EnumWebPersistableState.MAXIMAL_STATE_INFO));

			writeDebugMsg(logBuffer.toString());

			return webIServerSession;
		}
		catch (Exception e) {
//e.printStackTrace();
			logBuffer.append("Exception = ")
					.append(e.getMessage())
					.append("\n");
			LOG.error(methodName, e.getMessage(), e);
		}

		writeDebugMsg(logBuffer.toString());
		return null;
	}

	private static String validateSsoUser(ContainerServices cntSvcs) {
		String methodName = "validateSsoUser";
		if(debugLevel >= 5) {
			LOG.logger.logp(Level.INFO, LOG.logClassName, methodName, methodName + " invoked.");
		}
		StringBuffer logBuffer = new StringBuffer();
		if(debugLevel > 0) {
			logBuffer.append("validateSsoUser invoked").append("\n");
		}
		GenericCookie saasCookie = cntSvcs.getCookie(ssoCookieName);
		if(null == saasCookie) {
			LOG.error(methodName, String.format("Cookie %s not found.", ssoCookieName));
			logBuffer.append(String.format("Cookie %s not found.", ssoCookieName)).append("\n");
			writeDebugMsg(logBuffer.toString());
			return null;
		}
		String saasUiAuthKey = saasCookie.getValue();
		if(debugLevel >= 1) {
			LOG.info(methodName, "prov saas cookie value = " + saasUiAuthKey);
			logBuffer.append(String.format("prov saas cookie value = %s", saasUiAuthKey)).append("\n");
		}
		try {
			ProvUser user = webClient.get()
				.uri(ssoWebServiceUrl, singletonMap("token", saasUiAuthKey + ":" + ssoSecretKey))
//				.cookie("JSESSIONID", cntSvcs.getCookie("JSESSIONID").getValue())
				.retrieve()
				.bodyToMono(ProvUser.class)
				.block();
			logBuffer.append(String.format("User = %s", user.toString())).append("\n");
			if(!"OK".equalsIgnoreCase(user.getStatus())) {
				writeDebugMsg(logBuffer.toString());
				LOG.error(methodName, "error fetching user. user details: " + user);
				return null;
			}
			user.generateToken(ssoTokenPrefix);
			if(debugLevel >= 1) LOG.info(methodName, "Authorized user = " + user);
			String userToken = user.getToken();
			logBuffer.append(String.format("Token = %s", userToken)).append("\n");
			writeDebugMsg(logBuffer.toString());
			logBuffer.setLength(0);
			if(isEmpty(userToken)) {
				LOG.error(methodName, "token for user wasn't generated.");
				LOG.error(methodName, "user details: " + user.toString());
				return null;
			}
			// role check
			if(ssoEnableRoleCheck) {
				if(debugLevel >= 1) {
					LOG.info(methodName, "Role verification is enabled, checking for the role " + ssoRoleName);
					logBuffer.append(String.format("Role verification is enabled, checking for the role %s", ssoRoleName)).append("\n");
				}
				if(!user.hasRoleByName(ssoRoleName)) {
					if(debugLevel >= 1) {
						LOG.info(methodName, "Role verification failed, retuning invalid web iserver session.");
						logBuffer.append(String.format("Role verification failed, retuning invalid web iserver session.")).append("\n");
						writeDebugMsg(logBuffer.toString());
						return null;
					}
				}
			}
			cntSvcs.setSessionAttribute(SESSION_SSO_USER_TOKEN, userToken);
			return userToken;
		} catch(Exception e) {
//e.printStackTrace();
			LOG.error(methodName, "Failed to authorize the user; Error = " + e.getMessage(), e);
			cntSvcs.setSessionAttribute(SESSION_SSO_USER_TOKEN, null);
		}
		return null;
	}

	private void closeSession(String sessionState)
	{
		WebIServerSession webIServerSession = WebObjectsFactory.getInstance().getIServerSession();
		webIServerSession.restoreState(sessionState);
		closeSession(webIServerSession);
	}

	private void closeSession(WebIServerSession webIServerSession)
	{
		try {
			webIServerSession.closeSession();
		}
		catch (WebObjectsException e) {
			LOG.error("closeSession", "Exception during WebIServerSession close.", e);
		}
	}

	public static void writeDebugMsg(String text) {
		if(debugLevel>0) {
			File file = new File(debugLogFile);
			FileWriter fr = null;
			try {
				// Below constructor argument decides whether to append or override
				fr = new FileWriter(file, true);
				fr.write(text);
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			finally {
				try {
					fr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
