package tomia.prov.mstr.sso;

import com.microstrategy.utils.log.Level;
import com.microstrategy.utils.log.Logger;
import com.microstrategy.utils.log.LoggerConfigurator;

public class MstrLogWrapper extends LoggerConfigurator
{
	public Logger logger;
	public String logClassName;

	public MstrLogWrapper(Class clazz)
	{
		super();
		this.logClassName = clazz.getSimpleName();
		logger = createLogger();
	}

	public boolean isLoggable(Level theLevel)
	{
		return isLoggable(theLevel);
	}

	public void info(String method, String msg) {
		Level level = Level.INFO;
		log(level, method, msg);
	}

	public void error(String method, String msg)
	{
		log(Level.SEVERE, method, msg);
	}

	public void error(String method, String msg, Throwable t)
	{
		log(Level.SEVERE, method, msg, t);
	}

	private void log(Level level, String method, String msg)
	{
		logger.logp(level, logClassName, method, msg);
	}

	private void log(Level level, String method, String msg, Throwable t)
	{
		logger.logp(level, logClassName, method, msg, t);
	}

}
