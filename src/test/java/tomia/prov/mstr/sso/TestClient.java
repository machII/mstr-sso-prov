package tomia.prov.mstr.sso;

import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;

import reactor.netty.http.client.HttpClient;

import static java.util.Collections.singletonMap;

public class TestClient {

    public static void main(String[] args) {
// prov/verify-mstr-sso?value={tom_nsp_sso-value}:{internal-secret-key}
// http://localhost:8080/prov/verify-mstr-sso?value={token}
//        String ssoWebServiceUrl = "http://wsltkcham:8080/provisioning/rest/secure/510/verify-mstr-sso?value={token}";
        String ssoWebServiceUrl = "https://10.10.11.61:8443/sso-mst-710/rest/510/verify-mstr-sso?value={token}";
        String saasUiAuthKey = "saasUiAuthKey";
        String ssoSecretKey = "ssoSecretKey";
//        WebClient webClient = WebClient.create();
        try {
            SslContext sslContext = SslContextBuilder.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build();
            HttpClient httpClient = HttpClient.create().secure(scp -> scp.sslContext(sslContext));
            WebClient webClient = WebClient.builder().clientConnector(new ReactorClientHttpConnector(httpClient)).build();
            ProvUser user = webClient.get().uri(ssoWebServiceUrl, singletonMap("token", saasUiAuthKey + ":" + ssoSecretKey)).retrieve().bodyToMono(ProvUser.class).block();
            user.generateToken("prefix");
            System.out.println(user);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

}
