package tomia.prov.mstr.sso;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
public class TestController {

    private static final Logger log = LoggerFactory.getLogger(TestController.class);

    @GetMapping("/prov/verify-mstr-sso")
    public Mono<ProvUser> verify(String value) {
        log.info("Got token: {}", value);
        return Mono.fromSupplier(() -> generate(value));
    }

    ProvUser generate(String token) {
        ProvUser user = new ProvUser();
        if(token == null || token.length() < 4) {
            user.setStatus("ERR");
            return user;
        }
        user.setStatus("OK");
        user.setUserId(1234);
        user.setAccountId(5678);
        user.setUsername("John Doe");
        return user;
    }

    @GetMapping("/prov")
    public Mono<String> greet() {
        return Mono.just("Relax, you have been provisioned!");
    }

}
